package com.amirgb.etermaxtest.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amirgb.etermaxtest.BuildConfig;
import com.amirgb.etermaxtest.R;
import com.amirgb.etermaxtest.model.Photo;
import com.amirgb.etermaxtest.rest.ApiService;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 10/1/2016
 */

public class PhotoDetailActivity extends DefaultActivity {

    public static String PHOTO_INTENT = BuildConfig.APPLICATION_ID + ".intent.PHOTO";
    /**
     * Ui bindings
     */
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.imgPhoto)
    protected ImageView imgPhoto;
    @BindView(R.id.lblPhotoTitle)
    protected TextView lblPhotoTitle;
    @BindView(R.id.viewPhoto)
    protected View viewPhoto;
    @BindView(R.id.userPicture)
    protected ImageView userPicture;
    @BindView(R.id.lblOwnerName)
    protected TextView lblOwnerName;
    @BindView(R.id.imgViews)
    protected ImageView imgViews;
    @BindView(R.id.lblViews)
    protected TextView lblViews;
    @BindView(R.id.imgFaves)
    protected ImageView imgFaves;
    @BindView(R.id.lblFaves)
    protected TextView lblFaves;
    @BindView(R.id.imgComments)
    protected ImageView imgComments;
    @BindView(R.id.lblComments)
    protected TextView lblComments;
    @BindView(R.id.lblDescription)
    protected TextView lblDescription;
    @BindView(R.id.lblDate)
    protected TextView lblDate;
    @BindView(R.id.lblLocation)
    protected TextView lblLocation;
    @BindView(R.id.btnShowMap)
    protected Button btnShowMap;
    @BindView(R.id.viewLocation)
    protected View viewLocation;
    @BindView(R.id.viewDescription)
    protected CardView viewDescription;
    @BindView(R.id.imgMap)
    protected ImageView imgMap;

    private Photo mPhoto;
    private ApiService apiService;

    //---------------------------
    //  Activity show method
    //----------------------------

    public static void show(DefaultActivity activity, Photo photo) {
        activity.overridePendingTransition(R.anim.activity_transition_in, R.anim.activity_transition_out);
        activity.startActivity(new Intent(activity, PhotoDetailActivity.class)
                .putExtra(PHOTO_INTENT, photo));
    }

    //---------------------------
    //  Activity lifecycle
    //----------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    //---------------------------
    //  Activity overrides
    //---------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.activity_transition_in, R.anim.activity_transition_out);
        super.onBackPressed();
    }

    //---------------------------
    //  Default ativity overrides
    //---------------------------

    @Override
    protected void setupActivity() {
        Animation RightSwipe = AnimationUtils.loadAnimation(PhotoDetailActivity.this, R.anim.animation_from_left);
        Animation leftSwipe = AnimationUtils.loadAnimation(PhotoDetailActivity.this, R.anim.animation_from_right);

        viewPhoto.startAnimation(RightSwipe);
        viewDescription.startAnimation(leftSwipe);
        mPhoto = (Photo) getIntent().getSerializableExtra(PHOTO_INTENT);
        lblOwnerName.setText(mPhoto.getPhotoDetail().getOwner().getUsername());
        lblPhotoTitle.setText(mPhoto.getTitle() == null || mPhoto.getTitle().isEmpty() ?
                getString(R.string.lbl_photo_notitle) : mPhoto.getTitle());
        lblDescription.setText(Html.fromHtml(mPhoto.getPhotoDetail().getDescription().getContent()));
        lblViews.setText(mPhoto.getPhotoDetail().getViews());
        lblFaves.setText(mPhoto.getPhotoDetail().getCountFaves());
        lblComments.setText(mPhoto.getPhotoDetail().getComments().getContent());
        lblDate.setText(mPhoto.getPhotoDetail().getDates().getTaken());

        userPicture.setVisibility(mPhoto.getPhotoDetail().getOwner().getIconfarm().equals("0")
                && mPhoto.getPhotoDetail().getOwner().getIconserver().equals("0")
                ? View.GONE : View.VISIBLE);

        //validates if user has location to show in map
        if (mPhoto.getPhotoDetail().getLocation() != null) {
            btnShowMap.setVisibility(View.VISIBLE);
            imgMap.setVisibility(View.VISIBLE);
            StringBuilder region = new StringBuilder()
                    .append(mPhoto.getPhotoDetail().getLocation().getLocality() != null ? mPhoto.getPhotoDetail().getLocation().getLocality().getContent() : "")
                    .append(",")
                    .append(mPhoto.getPhotoDetail().getLocation().getCounty() != null ? mPhoto.getPhotoDetail().getLocation().getCounty().getContent() : "")
                    .append(mPhoto.getPhotoDetail().getLocation().getCounty() != null ? "," : "")
                    .append(mPhoto.getPhotoDetail().getLocation().getCountry() != null ? mPhoto.getPhotoDetail().getLocation().getCountry().getContent() : "");

            lblLocation.setText(region.toString());
        } else {
            btnShowMap.setVisibility(View.GONE);
            lblLocation.setVisibility(View.GONE);
            imgMap.setVisibility(View.GONE);
        }

        if (checkNetworkState(PhotoDetailActivity.this)) {
            apiService = new ApiService();
            apiService.loadPhoto(getApplicationContext(), imgPhoto, mPhoto, new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {

                }
            });

            apiService.loadUserPhoto(getApplicationContext(), userPicture, mPhoto.getPhotoDetail().getOwner(), new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    userPicture.setVisibility(View.GONE);
                }
            });
        } else {
            showNoInternetError(PhotoDetailActivity.this, true);
        }

    }

    //---------------------------
    //  Onclick method
    //---------------------------

    @OnClick(R.id.btnShowMap)
    public void onClick() {
        LocationActivity.show(PhotoDetailActivity.this, mPhoto.getPhotoDetail().getLocation());
    }

}
