package com.amirgb.etermaxtest.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.amirgb.etermaxtest.R;

import butterknife.ButterKnife;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 10/1/2016
 */

public abstract class DefaultActivity extends AppCompatActivity {


    private static AlertDialog alertDialog;


    //---------------------------
    //  Default set content view
    //-----------------------

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        overridePendingTransition(R.anim.activity_transition_in, R.anim.activity_transition_out);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setupActivity();
    }

    protected abstract void setupActivity();

    protected void showToast(String message) {
        Toast.makeText(DefaultActivity.this, message, Toast.LENGTH_LONG).show();
    }

    private void initDialog() {
        if (alertDialog == null)
            alertDialog = new AlertDialog.Builder(DefaultActivity.this, R.style.Dialog).create();
        else {
            alertDialog.dismiss();
            alertDialog = null;
            alertDialog = new AlertDialog.Builder(DefaultActivity.this, R.style.Dialog).create();
        }
    }

    protected void showOkDialog(String title, String message, final Runnable run) {
        initDialog();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.dlg_button_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (run != null) {
                            dialogInterface.dismiss();
                            run.run();
                        } else
                            dialogInterface.dismiss();
                    }
                });
        alertDialog.show();
    }


    protected void showProgressDialog(String message) {
        initDialog();
        alertDialog = new ProgressDialog(DefaultActivity.this, R.style.Dialog);
        alertDialog.setMessage(message);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissDialog() {
        alertDialog.dismiss();
    }

    protected boolean checkNetworkState(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infos[] = conMgr.getAllNetworkInfo();
        for (NetworkInfo info : infos) {
            if (info.getState() == NetworkInfo.State.CONNECTED)
                return true;
        }
        return false;
    }

    public void showConnectionError(final DefaultActivity activity, final boolean finish) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showOkDialog(getString(R.string.dlg_title_error), getString(R.string.error_no_connection), new Runnable() {
                    @Override
                    public void run() {
                        if (finish)
                            activity.finish();
                    }
                });
            }
        });
    }

    public void showNoInternetError(final DefaultActivity activity, final boolean finish) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showOkDialog(getString(R.string.dlg_title_error), getString(R.string.error_no_internet), new Runnable() {
                    @Override
                    public void run() {
                        if (finish)
                            activity.finish();
                    }
                });
            }
        });
    }
}
