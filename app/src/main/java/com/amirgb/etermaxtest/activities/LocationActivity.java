package com.amirgb.etermaxtest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.amirgb.etermaxtest.BuildConfig;
import com.amirgb.etermaxtest.R;
import com.amirgb.etermaxtest.model.Location;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;

public class LocationActivity extends DefaultActivity implements OnMapReadyCallback {
    /**
     * Intents
     **/
    public static String LOCATION_INTENT = BuildConfig.APPLICATION_ID + ".intent.LOCATION_INTENT";

    /**
     * UI Bindings
     **/
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /**
     * Map variables
     **/
    private GoogleMap mMap;
    private Location mLocation;

    //---------------------------
    //  Activity show method
    //----------------------------

    public static void show(DefaultActivity activity, Location location) {
        activity.overridePendingTransition(R.anim.activity_transition_in, R.anim.activity_transition_out);
        activity.startActivity(new Intent(activity, LocationActivity.class)
                .putExtra(LOCATION_INTENT, location));
    }

    //---------------------------
    //  Activity lifecycle
    //----------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    //---------------------------
    //  Activity overrides
    //---------------------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);

    }

    //---------------------------
    //  Default ativity overrides
    //---------------------------


    @Override
    protected void setupActivity() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mLocation = (Location) getIntent().getSerializableExtra(LOCATION_INTENT);

        mapFragment.getMapAsync(LocationActivity.this);
    }

    //---------------------------
    //  Bussiness logic
    // --------------------------

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng latLng = new LatLng(Double.parseDouble(mLocation.getLatitude()),
                Double.parseDouble(mLocation.getLongitude()));
        StringBuilder region = new StringBuilder()
                .append(mLocation.getLocality() != null ? mLocation.getLocality().getContent() : "")
                .append(",")
                .append(mLocation.getCounty() != null ? mLocation.getCounty().getContent() : "")
                .append(mLocation.getCounty() != null ? "," : "")
                .append(mLocation.getCountry() != null ? mLocation.getCountry().getContent() : "");

        mMap.addMarker(new MarkerOptions().position(latLng)
                .title(region.toString()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }
}
