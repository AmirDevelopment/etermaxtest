package com.amirgb.etermaxtest.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.amirgb.etermaxtest.R;
import com.amirgb.etermaxtest.adapters.IPhotoListAdapter;
import com.amirgb.etermaxtest.adapters.PhotoListAdapter;
import com.amirgb.etermaxtest.model.Photo;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotoDetails;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotos;
import com.amirgb.etermaxtest.rest.ApiService;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 10/1/2016
 */

public class PhotoListActivity extends DefaultActivity implements IPhotoListAdapter, SearchView.OnQueryTextListener {

    /**
     * UI binding
     **/

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.rcvPhotoList)
    protected RecyclerView rcvPhotoList;
    @BindView(R.id.swipeRefreshLayout)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.btnShowMore)
    protected Button btnShowMore;

    private SearchView mSearchView;


    private PhotoListAdapter mPhotoListAdapter;
    private ApiService mApiService;

    /**
     * Handle pagination variables
     **/
    private int page = 1;
    private final static int SHOW_PER_PAGE = 10;
    private int pageNumber;
    private AtomicBoolean showmore = new AtomicBoolean(false);

    private String query = "";

    private boolean isGridmode = false;

    private int defaultResId = R.layout.item_photo_list;
    //---------------------------
    //  Activity lifecycle
    //----------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //---------------------------
    //  Activity overrides
    //---------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_list, menu);
        // Associate searchable configuration with the SearchView
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setOnQueryTextListener(this);


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_change_grid:
                if (isGridmode) {
                    isGridmode = false;
                    defaultResId = R.layout.item_photo_list;
                    changeView(new LinearLayoutManager(this), defaultResId, -1);
                } else {
                    isGridmode = true;
                    defaultResId = R.layout.item_grid_photo_list;
                    changeView(new GridLayoutManager(this, 3), defaultResId,
                            getResources().getDisplayMetrics().densityDpi);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /** Search view methods**/
    @Override
    public boolean onQueryTextSubmit(String query) {
        searchPhotos(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (query != null && !query.equalsIgnoreCase(newText)) {
            page = 1;
        }
        return false;
    }

    //---------------------------
    //  Default ativity overrides
    //---------------------------

    @Override
    protected void setupActivity() {
        rcvPhotoList.setLayoutManager(new LinearLayoutManager(PhotoListActivity.this));
        rcvPhotoList.setHasFixedSize(true);
        rcvPhotoList.setAdapter(mPhotoListAdapter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                if (query.isEmpty())
                    getRecentPhotos();
                else
                    searchPhotos(query);
            }
        });

        getRecentPhotos();
    }

    //---------------------------
    //  Onclick method
    //-----------------------

    @OnClick(R.id.btnShowMore)
    public void onClick() {
        showmore.set(true);
        page++;
        StringBuilder message = new StringBuilder()
                .append("page ")
                .append(page)
                .append(" of ")
                .append(pageNumber);

        showToast(message.toString());

        if (query.isEmpty())
            getRecentPhotos();
        else
            searchPhotos(query);
    }

    //---------------------------
    //  Adapter interface
    // --------------------------

    /**
     * handle click on photo from the adapter
     *
     * @param photo
     */
    @Override
    public void onAdateperItemClick(Photo photo) {
        showPhotoDetail(photo);
    }

    @Override
    public void onAdapterError() {
    }

    //---------------------------
    //  Bussiness logic
    // --------------------------

    /**
     * Refresh the photos from flickr or adds more based in showmore variable
     */
    private void getRecentPhotos() {
        if (mApiService == null)
            mApiService = new ApiService();
        swipeRefreshLayout.setRefreshing(true);
        btnShowMore.setEnabled(false);
        if (checkNetworkState(PhotoListActivity.this)) {
            mApiService.getRecentPhotos(page, SHOW_PER_PAGE, new Callback<ResponsePhotos>() {
                @Override
                public void success(ResponsePhotos responsePhotos, Response response) {
                    //Update page number
                    pageNumber = responsePhotos.getPhotos().getPages();

                    if (pageNumber > 0 && page < pageNumber)
                        btnShowMore.setVisibility(View.VISIBLE);
                    else
                        btnShowMore.setVisibility(View.GONE);

                    //init adapter if not set
                    if (mPhotoListAdapter == null) {
                        mPhotoListAdapter = new PhotoListAdapter(PhotoListActivity.this,defaultResId);
                        rcvPhotoList.setAdapter(mPhotoListAdapter);
                    }

                    //preload details into list of photos
                    int size = responsePhotos.getPhotos().getPhoto().size();
                    for (int index = 0; index < size; index++) {
                        preLoadDetail(responsePhotos.getPhotos().getPhoto(), index);
                    }
                    // insert photos into adapter based on showmore variable
                    int beforeAddingSize = mPhotoListAdapter.getItemCount();
                    if (showmore.get()) {
                        mPhotoListAdapter.addAll(responsePhotos.getPhotos().getPhoto());
                        showmore.set(false);
                        rcvPhotoList.scrollToPosition(beforeAddingSize);
                    } else {
                        mPhotoListAdapter.setAll(responsePhotos.getPhotos().getPhoto());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    btnShowMore.setEnabled(true);
                    showConnectionError(PhotoListActivity.this, false);
                }
            });
        } else {
            showNoInternetError(PhotoListActivity.this, false);
            swipeRefreshLayout.setRefreshing(false);
            btnShowMore.setEnabled(true);
        }
    }


    /**
     * Search the photos that match with query from flickr or adds more based in showmore variable
     *
     */
    private void searchPhotos(String text) {
        query = text;
        if (mApiService == null)
            mApiService = new ApiService();
        swipeRefreshLayout.setRefreshing(true);
        btnShowMore.setEnabled(false);
        if (checkNetworkState(PhotoListActivity.this)) {
            mApiService.searchPhotos(page, SHOW_PER_PAGE, text, new Callback<ResponsePhotos>() {
                @Override
                public void success(ResponsePhotos responsePhotos, Response response) {
                    //Update page number
                    pageNumber = responsePhotos.getPhotos().getPages();

                    if (pageNumber > 0 && page < pageNumber)
                        btnShowMore.setVisibility(View.VISIBLE);
                    else
                        btnShowMore.setVisibility(View.GONE);

                    //init adapter if not set
                    if (mPhotoListAdapter == null) {
                        mPhotoListAdapter = new PhotoListAdapter(PhotoListActivity.this,defaultResId);
                        rcvPhotoList.setAdapter(mPhotoListAdapter);
                    }

                    //preload details into list of photos
                    int size = responsePhotos.getPhotos().getPhoto().size();
                    for (int index = 0; index < size; index++) {
                        preLoadDetail(responsePhotos.getPhotos().getPhoto(), index);
                    }
                    // insert photos into adapter based on showmore variable
                    int beforeAddingSize = mPhotoListAdapter.getItemCount();
                    if (showmore.get()) {
                        mPhotoListAdapter.addAll(responsePhotos.getPhotos().getPhoto());
                        showmore.set(false);
                        rcvPhotoList.scrollToPosition(beforeAddingSize);
                    } else {
                        mPhotoListAdapter.setAll(responsePhotos.getPhotos().getPhoto());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    btnShowMore.setEnabled(true);
                    showConnectionError(PhotoListActivity.this, false);
                }
            });
        } else {
            showNoInternetError(PhotoListActivity.this, false);
            swipeRefreshLayout.setRefreshing(false);
            btnShowMore.setEnabled(true);
        }
    }

    /**
     * opens activity of photo details
     *
     * @param photo
     */
    private void showPhotoDetail(final Photo photo) {
        showProgressDialog(getString(R.string.dlg_message_loading));
        if (checkNetworkState(PhotoListActivity.this)) {

            mApiService.getPhotoDetail(photo.getId(), new Callback<ResponsePhotoDetails>() {
                @Override
                public void success(ResponsePhotoDetails responsePhotoDetails, Response response) {
                    photo.setPhotoDetail(responsePhotoDetails.getPhoto());
                    dismissDialog();
                    if (responsePhotoDetails.getPhoto() != null)
                        PhotoDetailActivity.show(PhotoListActivity.this, photo);

                }

                @Override
                public void failure(RetrofitError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    btnShowMore.setEnabled(true);
                    showConnectionError(PhotoListActivity.this, false);
                    dismissDialog();
                }
            });
        } else {
            showNoInternetError(PhotoListActivity.this, false);
            swipeRefreshLayout.setRefreshing(false);
            btnShowMore.setEnabled(true);
        }
    }

    /**
     * preload photo detail into adapter to show comments,favs,views and if it has location available
     *
     * @param photos
     * @param position
     */
    private void preLoadDetail(final List<Photo> photos, final int position) {

        if (checkNetworkState(PhotoListActivity.this)) {
            if (mApiService == null)
                mApiService = new ApiService();
            swipeRefreshLayout.setRefreshing(true);
            btnShowMore.setEnabled(false);
            mApiService.getPhotoDetail(photos.get(position).getId(), new Callback<ResponsePhotoDetails>() {
                @Override
                public void success(ResponsePhotoDetails responsePhotoDetails, Response response) {
                    photos.get(position).setPhotoDetail(responsePhotoDetails.getPhoto());
                    swipeRefreshLayout.setRefreshing(false);
                    btnShowMore.setEnabled(true);
                }

                @Override
                public void failure(RetrofitError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    btnShowMore.setEnabled(true);
                    showConnectionError(PhotoListActivity.this, false);
                }
            });
        } else {
            showNoInternetError(PhotoListActivity.this, false);
            swipeRefreshLayout.setRefreshing(false);
            btnShowMore.setEnabled(true);
        }
    }

    /**
     * Updates photo grid layout
     * @param layoutManage
     * @param view
     * @param dpi
     */
    private void changeView(final RecyclerView.LayoutManager layoutManage, final int view,
                            final int dpi) {

        List<Photo> photoList = mPhotoListAdapter.getAll();

        rcvPhotoList.setLayoutManager(layoutManage);
        rcvPhotoList.setItemAnimator(new DefaultItemAnimator());

        mPhotoListAdapter = new PhotoListAdapter(PhotoListActivity.this, view);
        mPhotoListAdapter.setAll(photoList);
        rcvPhotoList.setAdapter(mPhotoListAdapter);
        rcvPhotoList.setHasFixedSize(true);
    }
}
