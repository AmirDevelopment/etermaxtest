package com.amirgb.etermaxtest.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Dates implements Serializable {
    private String taken;

    public Dates(String taken) {
        this.taken = taken;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }
}
