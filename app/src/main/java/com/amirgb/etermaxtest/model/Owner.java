package com.amirgb.etermaxtest.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Owner implements Serializable {
    private String username;
    private String iconserver;
    private String nsid;
    private String iconfarm;

    public Owner(String username, String iconserver, String nsid, String iconfarm) {
        this.username = username;
        this.iconserver = iconserver;
        this.nsid = nsid;
        this.iconfarm = iconfarm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIconserver() {
        return iconserver;
    }

    public void setIconserver(String iconserver) {
        this.iconserver = iconserver;
    }

    public String getNsid() {
        return nsid;
    }

    public void setNsid(String nsid) {
        this.nsid = nsid;
    }

    public String getIconfarm() {
        return iconfarm;
    }

    public void setIconfarm(String iconfarm) {
        this.iconfarm = iconfarm;
    }
}
