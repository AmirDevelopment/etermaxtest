package com.amirgb.etermaxtest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Description implements Serializable {
    @SerializedName("_content")
    private String content;

    public Description(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
