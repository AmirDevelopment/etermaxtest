package com.amirgb.etermaxtest.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Tags implements Serializable {
    private List<Tag> tag;

    public Tags(List<Tag> tag) {
        this.tag = tag;
    }

    public List<Tag> getTag() {
        return tag;
    }

    public void setTag(List<Tag> tag) {
        this.tag = tag;
    }
}
