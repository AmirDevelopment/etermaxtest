package com.amirgb.etermaxtest.model.Responses;


import com.amirgb.etermaxtest.model.PhotoDetail;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class ResponsePhotoDetails implements Serializable {
    private PhotoDetail photo;

    public ResponsePhotoDetails(PhotoDetail photo) {
        this.photo = photo;
    }

    public PhotoDetail getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoDetail photo) {
        this.photo = photo;
    }
}
