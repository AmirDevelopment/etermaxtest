package com.amirgb.etermaxtest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class PhotoDetail implements Serializable {
    private Description description;
    private String views;
    @SerializedName("count_faves")
    private String countFaves;
    private Comments comments;
    private Owner owner;
    private Location location;
    private Tags tags;
    private Dates dates;

    public PhotoDetail(Description description, String views, String countFaves, Comments comments, Owner owner, Location location, Tags tags, Dates dates) {
        this.description = description;
        this.views = views;
        this.countFaves = countFaves;
        this.comments = comments;
        this.owner = owner;
        this.location = location;
        this.tags = tags;
        this.dates = dates;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getCountFaves() {
        return countFaves;
    }

    public void setCountFaves(String countFaves) {
        this.countFaves = countFaves;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public Dates getDates() {
        return dates;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }
}
