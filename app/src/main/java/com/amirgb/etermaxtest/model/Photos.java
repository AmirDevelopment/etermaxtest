package com.amirgb.etermaxtest.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Photos implements Serializable {

    private List<Photo> photo;
    private int page;
    private int pages;
    private int perpage;
    private int total;

    public Photos(List<Photo> photo, int page, int pages, int perpage, int total) {
        this.photo = photo;
        this.page = page;
        this.pages = pages;
        this.perpage = perpage;
        this.total = total;
    }

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
