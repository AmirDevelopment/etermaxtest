package com.amirgb.etermaxtest.model.Responses;


import com.amirgb.etermaxtest.model.Photos;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class ResponsePhotos implements Serializable {
    private Photos photos;

    public ResponsePhotos(Photos photos) {
        this.photos = photos;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }
}
