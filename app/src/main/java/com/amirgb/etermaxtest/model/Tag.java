package com.amirgb.etermaxtest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Tag implements Serializable {
    @SerializedName("_content")
    String content;
    String raw;

    public Tag(String content, String raw) {
        this.content = content;
        this.raw = raw;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }
}