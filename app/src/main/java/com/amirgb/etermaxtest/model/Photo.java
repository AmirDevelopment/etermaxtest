package com.amirgb.etermaxtest.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class Photo implements Serializable {

    private String id;
    private int farm;
    private String secret;
    private String server;
    private String title;
    private PhotoDetail photoDetail;

    public Photo(String id, int farm, String secret, String server, String title, PhotoDetail photoDetail) {
        this.id = id;
        this.farm = farm;
        this.secret = secret;
        this.server = server;
        this.title = title;
        this.photoDetail = photoDetail;
    }

    public Photo(String id, int farm, String secret, String title) {
        this.id = id;
        this.farm = farm;
        this.secret = secret;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PhotoDetail getPhotoDetail() {
        return photoDetail;
    }

    public void setPhotoDetail(PhotoDetail photoDetail) {
        this.photoDetail = photoDetail;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
}
