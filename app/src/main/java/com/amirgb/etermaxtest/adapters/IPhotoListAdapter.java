package com.amirgb.etermaxtest.adapters;

import com.amirgb.etermaxtest.model.Photo;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 10/1/2016
 */

public interface IPhotoListAdapter {

    void onAdateperItemClick(Photo photo);

    void onAdapterError();

}
