package com.amirgb.etermaxtest.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amirgb.etermaxtest.R;
import com.amirgb.etermaxtest.model.Photo;
import com.amirgb.etermaxtest.rest.ApiService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 10/1/2016
 */

public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.PhotoListViewHolder> {


    private List<Photo> mPhotoList = new ArrayList<>();
    private Context mContext;
    private IPhotoListAdapter mInterfaceAdapter;
    private int mResId;

    //-----------------------
    //  Constructors
    //-----------------------

    public PhotoListAdapter(AppCompatActivity context, int resId) {
        this.mContext = context;
        this.mInterfaceAdapter = (IPhotoListAdapter) context;
        this.mResId = resId;
    }

    //-----------------------
    //  List methods
    //-----------------------

    /**
     * replace all the elements
     * @param photoList
     */
    public void setAll(@NonNull
                       final List<Photo> photoList) {
        mPhotoList.clear();
        mPhotoList.addAll(photoList);
        notifyDataSetChanged();
    }

    /**
     * adds all the elements to current stack
     * @param photoList
     */
    public void addAll(@NonNull
                       final List<Photo> photoList) {
        for (Photo photo : photoList)
            mPhotoList.add(photo);
        notifyDataSetChanged();

    }

    public List<Photo> getAll() {
       return mPhotoList;
    }

    @Override
    public int getItemCount() {
        return mPhotoList != null ? mPhotoList.size() : 0;
    }


    //-----------------------
    //  Adapter methods
    //-----------------------
    @Override
    public PhotoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PhotoListViewHolder vh = new PhotoListViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(mResId, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(PhotoListViewHolder holder, int position) {
        final Photo photo = mPhotoList.get(position);
        holder.lblPhotoTitle.setText(photo.getTitle() == null || photo.getTitle().isEmpty() ?
                mContext.getString(R.string.lbl_photo_notitle) : photo.getTitle());
        //Show stats in adapter
        if (photo.getPhotoDetail() != null) {
            holder.lblViews.setText(photo.getPhotoDetail().getViews());
            holder.lblFaves.setText(photo.getPhotoDetail().getCountFaves());
            holder.lblComments.setText(photo.getPhotoDetail().getComments().getContent());
            holder.imgMap.setVisibility(photo.getPhotoDetail().getLocation() != null ? View.VISIBLE : View.GONE);
        }

        ApiService apiService = new ApiService();
        apiService.loadPhoto(mContext.getApplicationContext(), holder.imgPhoto, photo, new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
            }
        });
    }


    //-----------------------
    //  View holder
    //-----------------------
    public final class PhotoListViewHolder extends RecyclerView.ViewHolder {
        /**
         * Ui bindings
         **/
        @BindView(R.id.imgPhoto)
        protected ImageView imgPhoto;
        @BindView(R.id.lblPhotoTitle)
        protected TextView lblPhotoTitle;
        @BindView(R.id.viewPhoto)
        protected View viewPhoto;
        @BindView(R.id.imgViews)
        protected ImageView imgViews;
        @BindView(R.id.lblViews)
        protected TextView lblViews;
        @BindView(R.id.imgFaves)
        protected ImageView imgFaves;
        @BindView(R.id.lblFaves)
        protected TextView lblFaves;
        @BindView(R.id.imgComments)
        protected ImageView imgComments;
        @BindView(R.id.imgMap)
        protected ImageView imgMap;
        @BindView(R.id.lblComments)
        protected TextView lblComments;

        public PhotoListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        //-----------------------
        //  onClickMethod
        //-----------------------
        @OnClick(R.id.viewPhoto)
        public void onClick() {
            mInterfaceAdapter.onAdateperItemClick(mPhotoList.get(getAdapterPosition()));
        }
    }

}
