package com.amirgb.etermaxtest.rest;


import android.content.Context;
import android.widget.ImageView;

import com.amirgb.etermaxtest.BuildConfig;
import com.amirgb.etermaxtest.CircleTransform;
import com.amirgb.etermaxtest.model.Owner;
import com.amirgb.etermaxtest.model.Photo;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotoDetails;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotos;
import com.amirgb.etermaxtest.rest.Services.IPhotos;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public class ApiService implements IPhotos {
    private static RestAdapter mRestAdapter;
    private static FlickrService mFlickrService;
    private static Picasso picasso;

    private static final String METHOD_GET_RECENT_PHOTOS = "flickr.photos.getRecent";
    private static final String METHOD_SEARCH_PHOTOS = "flickr.photos.search";
    private static final String METHOD_GET_PHOTO_INFO = "flickr.photos.getInfo";
    private static final String NOJSONCALLBACK = "1";
    private static final String EXTRAS_COUNT_FAVES = "count_faves";

    static {
        com.squareup.okhttp.OkHttpClient client = new com.squareup.okhttp.OkHttpClient();
        if (BuildConfig.DEBUG) {
            client.networkInterceptors().add(new com.facebook.stetho.okhttp.StethoInterceptor());
        }
        mRestAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(client))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setEndpoint(BuildConfig.API_URL).build();
        mFlickrService = mRestAdapter.create(FlickrService.class);
    }

    private void initPicasso(Context context, Picasso.Listener listener) {

        com.squareup.okhttp.OkHttpClient client = new com.squareup.okhttp.OkHttpClient();
        client.networkInterceptors().add(new com.facebook.stetho.okhttp.StethoInterceptor());

        if (picasso == null) {

            picasso = new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .loggingEnabled(BuildConfig.DEBUG)
                    .indicatorsEnabled(BuildConfig.DEBUG)
                    .listener(listener)
                    .build();
            Picasso.setSingletonInstance(picasso);
        }
    }

    @Override
    public void getRecentPhotos(int page, int perPage, Callback<ResponsePhotos> responsePhotosCallback) {
        mFlickrService.getRecent(METHOD_GET_RECENT_PHOTOS, BuildConfig.API_KEY, BuildConfig.API_FORMAT, NOJSONCALLBACK, page, perPage, responsePhotosCallback);
    }

    @Override
    public void searchPhotos(int page, int perPage, String text, Callback<ResponsePhotos> responsePhotosCallback) {
        mFlickrService.searchPhotos(METHOD_SEARCH_PHOTOS, BuildConfig.API_KEY, BuildConfig.API_FORMAT, NOJSONCALLBACK, page, perPage, text,responsePhotosCallback);
    }

    @Override
    public void getPhotoDetail(String photoId, Callback<ResponsePhotoDetails> responsePhotoDetailsCallback) {
        mFlickrService.getPhotoDetail(METHOD_GET_PHOTO_INFO, BuildConfig.API_KEY, BuildConfig.API_FORMAT, NOJSONCALLBACK, photoId, EXTRAS_COUNT_FAVES, responsePhotoDetailsCallback);
    }

    @Override
    public void loadPhoto(Context context, ImageView view, Photo photo, Picasso.Listener listener) {
        initPicasso(context, listener);
        StringBuilder url = new StringBuilder()
                .append("https://farm")
                .append(photo.getFarm())
                .append(".staticflickr.com/")
                .append(photo.getServer())
                .append("/")
                .append(photo.getId())
                .append("_")
                .append(photo.getSecret())
                .append(".jpg");


        Picasso.with(context)
                .load(url.toString())
                //  .networkPolicy(NetworkPolicy.OFFLINE)
                .fit().into(view);

    }

    @Override
    public void loadUserPhoto(Context context, ImageView view, Owner owner, Picasso.Listener listener) {
        initPicasso(context, listener);
        StringBuilder url = new StringBuilder()
                .append("https://farm")
                .append(owner.getIconfarm())
                .append(".staticflickr.com/")
                .append(owner.getIconserver())
                .append("/buddyicons/")
                .append(owner.getNsid())
                .append(".jpg");

        Picasso.with(context)
                .load(url.toString())
                .transform(new CircleTransform())
                //  .networkPolicy(NetworkPolicy.OFFLINE)
                .into(view);
    }


}
