package com.amirgb.etermaxtest.rest;


import com.amirgb.etermaxtest.model.Responses.ResponsePhotoDetails;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotos;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public interface FlickrService {

    @GET("/")
    void getRecent(@Query("method") String method, @Query("api_key") String apiKey,
                   @Query("format") String format, @Query("nojsoncallback") String mpJsonCallback,
                   @Query("page") int page,
                   @Query("per_page") int perpage,
                   Callback<ResponsePhotos> responsePhotosCallback);


    @GET("/")
    void searchPhotos(@Query("method") String method, @Query("api_key") String apiKey,
                      @Query("format") String format, @Query("nojsoncallback") String mpJsonCallback,
                      @Query("page") int page,
                      @Query("per_page") int perpage,
                      @Query("text") String text,
                      Callback<ResponsePhotos> responsePhotosCallback);

    @GET("/")
    void getPhotoDetail(@Query("method") String method, @Query("api_key") String apiKey,
                        @Query("format") String format, @Query("nojsoncallback") String mpJsonCallback,
                        @Query("photo_id") String photoId, @Query("extras") String extras,
                        Callback<ResponsePhotoDetails> responsePhotoDetailsCallback);


}
