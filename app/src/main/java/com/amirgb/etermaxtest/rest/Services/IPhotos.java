package com.amirgb.etermaxtest.rest.Services;


import android.content.Context;
import android.widget.ImageView;

import com.amirgb.etermaxtest.model.Owner;
import com.amirgb.etermaxtest.model.Photo;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotoDetails;
import com.amirgb.etermaxtest.model.Responses.ResponsePhotos;
import com.squareup.picasso.Picasso;

import retrofit.Callback;

/**
 * Created by Amir Granadillo
 * email: a.g.2005.13@gmail.com
 * date: 9/29/2016
 */

public interface IPhotos {

    void getRecentPhotos(final int page, final int perPage, Callback<ResponsePhotos> responsePhotosCallback);

    void searchPhotos(final int page, final int perPage, String text, Callback<ResponsePhotos> responsePhotosCallback);

    void getPhotoDetail(final String photoId, Callback<ResponsePhotoDetails> ResponsePhotoDetails);

    void loadPhoto(Context context, ImageView view, final Photo photo, Picasso.Listener listener);

    void loadUserPhoto(Context context, ImageView view, final Owner owner, Picasso.Listener listener);

}
